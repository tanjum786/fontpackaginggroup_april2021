(function () {
  var HIDE_FOCUS_STYLES_CLASS = 'disable-focus-styles';
  var SHOW_FOCUS_STYLES_CLASS = 'enable-focus-styles';
  function domReady(callback) {
    if (['interactive', 'complete'].indexOf(document.readyState) >= 0) {
      callback();
    } else {
      document.addEventListener('DOMContentLoaded', callback);
    }
  }
  $('input[name="globalunsub"]').change(function () {
    if ($(this).is(':checked')) {
      $('.item').addClass('disabled')
      $('.item input').attr('disabled', 'disabled')
    }
    else {
      $('.item').removeClass('disabled')
      $('.item input').removeAttr('disabled')
    }
  });
})();
document.addEventListener("DOMContentLoaded", function(event) {
  var element = document.querySelector('.blog-sidebar');
  if( element ) {
    function resize() {
      if (window.innerWidth < 768) {
        element.classList.add('mobile');
      } else {
        element.classList.remove('mobile');
      }
    }
  }
  window.onload = resize;
  window.onresize = resize;
  var topicListing = document.querySelector('.blog-sidebar-topic-filter h3');
  var topicListingSidebar = document.querySelector('.blog-sidebar-topic-filter');
  var postListing = document.querySelector('.blog-sidebar-post-listing h3');
  var postListingSidebar = document.querySelector('.blog-sidebar-post-listing');
  if(postListing) {
    postListing.addEventListener('click', function (event){
      postListingSidebar.classList.toggle ('clicked');
    });
  }
  if(topicListing) {
    topicListing.addEventListener('click', function (event){
      topicListingSidebar.classList.toggle ('clicked');
    });
  }
});

$( document ).ready(function() {
  $("#toTop").hide();
  $(window).scroll(function(){
    if ($(window).scrollTop()>50){
      $("#toTop").fadeIn(500);
    }
    else
    {
      $("#toTop").fadeOut(500);
    }
  });
  $("#toTop").click(function(){
    $('body,html').animate({scrollTop:0},500);
    return false;
  });
  
  $( ".search-mobile" ).click(function() {
    $( "#search-input-mobile" ).slideToggle();
  });
});

$(window).scroll(function(){
  var distanceY = window.pageYOffset || document.documentElement.scrollTop,
      shrinkOn = 300,
      body = document.querySelector("body");
  if ($(this).scrollTop() > 161){
    $('body').addClass("scroll-header");
  }
  else{
    $('body').removeClass("scroll-header");
  }
});

$(".custom-menu-primary.mobile_menu .hs-item-has-children > a").after(' <div class="child-trigger"><i></i></div>'),
$(".fusion-mobile-menu-icons").click((function() {
    $(".has-submenu").removeClass("child-open"),
    $(".custom-menu-primary").slideToggle(250),
    $("body").toggleClass("mobile-open")
}
)),
$(".child-trigger").click((function() {
    return $(this).parent().siblings(".hs-item-has-children").removeClass("child-open"),
    $(this).parent().siblings(".hs-item-has-children").find(".custom-menu-primary .navigation-primary .submenu .menu-item ul").slideUp(250),
    $(this).next(".custom-menu-primary .navigation-primary .submenu .menu-item ul").slideToggle(250),
    $(this).next(".hs-menu-children-wrapper").children(".hs-item-has-children").find(".custom-menu-primary .navigation-primary .submenu .menu-item ul").slideUp(250),
    $(this).next(".hs-menu-children-wrapper").children(".hs-item-has-children").find(".child-trigger").removeClass("child-open"),
    $(this).toggleClass("child-open"),
    !1
}
));

