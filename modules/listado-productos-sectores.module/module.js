function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}
 
// Search
$('#search-productos').keyup(delay(function(e){
  
  //var textos = $(this).val();

  var query1 = buildQuery();
  getProductos(query1);
 
}, 500));

function getProductos(query) {
  $.ajax({
      type: 'GET',
      url: 'https://api.hubapi.com/cms/v3/hubdb/tables/5307489/rows',
      data: 'portalId=9149224' + query,
    }).done(function (respuesta) {
    //var resultados = JSON.stringify(respuesta.results[0].score);
    //console.log(respuesta.results);
    //console.log(respuesta);
    $("#listado-productos").hide();
    
    //respuesta.results.sort((a,b) => (b.values.orden > a.values.orden) ? 1 : ((a.values.orden > b.values.orden) ? -1 : 0));
    
    if(respuesta.results.length > 0) {
      $("#search-results").html("");
      $("#nothing-found").hide();
      $('#search-results').show();
       for(var i=0; i<respuesta.results.length; i++){
       
         var lang = document.documentElement.lang;
         var name = respuesta.results[i].values.name;
         var url = respuesta.results[i].path;
         var image = respuesta.results[i].values.imagen;
         var imageUrl = "https://f.hubspotusercontent20.net/hubfs/9149224/raw_assets/public/INSUPACKFontPackagingGroup_April2021/images/PlaceholderProductos.png";
         var slug_products = "productos-por-sector";
         var slug_lang = "";
         
         if(lang == "en") {
           slug_lang = "/en";
           slug_products = "products-by-sector";
           name = respuesta.results[i].values.nombre_en;
         }
         
         if(lang == "fr") {
           slug_lang = "/fr";
           slug_products = "produits-par-secteur";
           name = respuesta.results[i].values.nombre_fr;
         }
         
         if(lang == "ca") {
           slug_lang = "/ca";
           slug_products = "productes-per-sector";
           name = respuesta.results[i].values.nombre_ca;
         }

         if(image) {
           imageUrl = image.url;
         }
         
         
         var contenido = '<div class="item-producto">'
         + '<div class="box-producto">'
         + '<a href="'+ slug_lang +'/'+ slug_products +'/'+ url +'" class="box">'
         + '<img src="'+ imageUrl +'" alt="'+ name + '">'
         + '<h4 class="mt10"><strong>'+ name +'</strong></h4>'
         + '</a>'
         + '</div></div>';
         
         $('#search-results').append(contenido);
      }
    } else {
      $('#search-results').hide();
      $("#nothing-found").show();
    }
    
  }).fail(function (msg) {
    console.log('FAIL');
  });
}

function buildQuery() {
  var querySearch = "&name__icontains=" + $('#search-productos').val();
  var lang = document.documentElement.lang;
  
  if(lang == "es") {
    var querySearch = "&name__icontains=" + $('#search-productos').val();
  }

  if(lang == "en") {
    var querySearch = "&nombre_en__icontains=" + $('#search-productos').val();
  }

  if(lang == "fr") {
    var querySearch = "&nombre_fr__icontains=" + $('#search-productos').val();
  }

  if(lang == "ca") {
    var querySearch = "&nombre_ca__icontains=" + $('#search-productos').val();
  }
  
  var selectedSector = $('#sectores').children("option:selected").val();
  var querySector = "";
  
  if(selectedSector) {
    
    if(lang == "es") {
      var querySector = "&sector__contains=" + selectedSector;
    }
    
    if(lang == "en") {
      var querySector = "&sector_en__contains=" + selectedSector;
    }
    
    if(lang == "fr") {
      var querySector = "&sector_fr__contains=" + selectedSector;
    }
    
    if(lang == "ca") {
      var querySector = "&sector_ca__contains=" + selectedSector;
    }
  }
  
  var queryFilters = querySector;
  var query = queryFilters ;
  
  if ($('#search-productos').val().length > 2) {
    query = querySearch + queryFilters;
  }
  
  return query + "&sort=random()";
}

// Filter
$("select").change(function(){
  
  var query = buildQuery();
  
  getProductos(query);
  
});

