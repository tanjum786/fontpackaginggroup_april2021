// Function to update the table
var currentPage = 1;
var itemsPerPage = 5;

const loader = document.getElementById("loader");
const showLoader = () => {
  loader.classList.remove("loader-hidden");
};

const hideLoader = () => {
  loader.classList.add("loader-hidden");
};

function updateTable(data) {
  try {
    var parentDiv = document.getElementById("table_container-pedidos");
    var childDiv = document.createElement("div");
    childDiv.classList.add("table_wrapper-pedidos");
    parentDiv.innerHTML = "";
    var start = (currentPage - 1) * itemsPerPage;
    var end = start + itemsPerPage;
    var pageData = data.slice(start, end);
    pageData?.forEach((item) => {
      var itemDiv = document.createElement("div");
      itemDiv.classList.add("data_pedidos");
      var combaineConatiner = document.createElement("div");
      combaineConatiner.classList.add("combaine_conatiner");

      var singleItemDiv1 = document.createElement("div");
      singleItemDiv1.classList.add("single-data_pedidos");

      var spanEmpresaTitle = document.createElement("span");
      spanEmpresaTitle.textContent = `Empresa:`;
      spanEmpresaTitle.classList.add("padidos-title");

      var spanEmpresa = document.createElement("span");
      spanEmpresa.textContent = item?.properties?.empresa || "-";

      singleItemDiv1.appendChild(spanEmpresaTitle);
      singleItemDiv1.appendChild(spanEmpresa);

      itemDiv.appendChild(singleItemDiv1);

      var singleItemDiv2 = document.createElement("div");
      singleItemDiv2.classList.add("single-data_pedidos");

      var spanPadidosIdTitle = document.createElement("span");
      spanPadidosIdTitle.textContent = "ID Pedido Cliente:";
      spanPadidosIdTitle.classList.add("padidos-title");

      var spanIdPadidos = document.createElement("span");
      spanIdPadidos.textContent = item?.properties?.id_pedido_cliente || "-";

      singleItemDiv2.appendChild(spanPadidosIdTitle);
      singleItemDiv2.appendChild(spanIdPadidos);

      itemDiv.appendChild(singleItemDiv2);

      var singleItemDiv3 = document.createElement("div");
      singleItemDiv3.classList.add("single-data_pedidos", "hide-element");

      var spanReferenciaTitle = document.createElement("span");
      spanReferenciaTitle.textContent = "Referencia:";
      spanReferenciaTitle.classList.add("padidos-title");

      var spanReferencia = document.createElement("span");
      spanReferencia.textContent =
        item?.properties?.referencia_del_cliente || "-";

      singleItemDiv3.appendChild(spanReferenciaTitle);
      singleItemDiv3.appendChild(spanReferencia);

      itemDiv.appendChild(singleItemDiv3);

      var singleItemDiv4 = document.createElement("div");
      singleItemDiv4.classList.add("single-data_pedidos", "hide-element");

      var spanCantidadTitle = document.createElement("span");
      spanCantidadTitle.textContent = "Cantidad:";
      spanCantidadTitle.classList.add("padidos-title");

      var spanCantidad = document.createElement("span");
      spanCantidad.textContent = item?.properties?.cantidad_pedido || "-";

      singleItemDiv4.appendChild(spanCantidadTitle);
      singleItemDiv4.appendChild(spanCantidad);

      itemDiv.appendChild(singleItemDiv4);

      var singleItemDiv5 = document.createElement("div");
      singleItemDiv5.classList.add("single-data_pedidos", "hide-element");

      var spanentregaTitle = document.createElement("span");
      spanentregaTitle.textContent = "Fecha de entrega:";
      spanentregaTitle.classList.add("padidos-title");

      var spanentrega = document.createElement("span");
      spanentrega.textContent = item?.properties?.fecha_entrega || "-";

      singleItemDiv5.appendChild(spanentregaTitle);
      singleItemDiv5.appendChild(spanentrega);

      itemDiv.appendChild(singleItemDiv5);

      var singleItemDiv6 = document.createElement("div");
      singleItemDiv6.classList.add("single-data_pedidos", "hide-element");

      var spanEstadoTitle = document.createElement("span");
      spanEstadoTitle.textContent = "Estado:";
      spanEstadoTitle.classList.add("padidos-title");

      var statusContainer = document.createElement("div");
      statusContainer.classList.add("status-container");
      var spanEstado = document.createElement("span");
      spanEstado.textContent = item?.properties?.estado || "-";
      var spanStatus = document.createElement("div");
      spanStatus.classList.add("status");

      statusContainer.appendChild(spanStatus);
      statusContainer.appendChild(spanEstado);

      singleItemDiv6.appendChild(spanEstadoTitle);
      singleItemDiv6.appendChild(statusContainer);

      itemDiv.appendChild(singleItemDiv6);

      var singleItemDiv7 = document.createElement("div");
      singleItemDiv7.classList.add("single-data_pedidos", "accorion-div");

      var accordionBtn = document.createElement("button");
      accordionBtn.classList.add("accordion");

      var img = document.createElement("img");
      img.src =
        "https://9149224.fs1.hubspotusercontent-na1.net/hubfs/9149224/Area%20privada%20-%20FONT/Vector%2015.svg";
      img.alt = "";

      accordionBtn.appendChild(img);
      singleItemDiv7.appendChild(accordionBtn);

      var originalImgSrc =
        "https://9149224.fs1.hubspotusercontent-na1.net/hubfs/9149224/Area%20privada%20-%20FONT/Vector%2015.svg";
      var newImgSrc =
        "https://9149224.fs1.hubspotusercontent-na1.net/hubfs/9149224/Area%20privada%20-%20FONT/Vector%2015%20(1).svg";

      let accordionContent = document.createElement("div");
      accordionContent.classList.add("accordion-container");

      let accordionContentWrapper = document.createElement("div");
      accordionContentWrapper.classList.add("accordion-container-wrapper");

      let accordionColumn1 = document.createElement("div");
      accordionColumn1.classList.add("accordion-column");

      let accordionItem = document.createElement("div");
      accordionItem.classList.add("accordion-item");

      let accordionSpanTitle1 = document.createElement("span");
      accordionSpanTitle1.classList.add("accordion-item_title");
      accordionSpanTitle1.textContent = "ID Pedido Font:";

      let accordionSpan1 = document.createElement("span");
      accordionSpan1.classList.add("accordion-item_span", "bold-item");
      accordionSpan1.textContent = item?.properties?.id_pedido_font || "-";

      accordionItem.appendChild(accordionSpanTitle1);
      accordionItem.appendChild(accordionSpan1);
      accordionColumn1.appendChild(accordionItem);

      let accordionItem2 = document.createElement("div");
      accordionItem2.classList.add("accordion-item");

      let accordionSpanTitle2 = document.createElement("span");
      accordionSpanTitle2.classList.add("accordion-item_title");
      accordionSpanTitle2.textContent = "ID Cliente Font:";

      let accordionSpan2 = document.createElement("span");
      accordionSpan2.classList.add("accordion-item_span");
      accordionSpan2.textContent = item?.properties?.id_cliente_font || "-";

      accordionItem2.appendChild(accordionSpanTitle2);
      accordionItem2.appendChild(accordionSpan2);
      accordionColumn1.appendChild(accordionItem2);

      let accordionItem3 = document.createElement("div");
      accordionItem3.classList.add("accordion-item");

      let accordionSpanTitle3 = document.createElement("span");
      accordionSpanTitle3.classList.add("accordion-item_title");
      accordionSpanTitle3.textContent = "Línea pedido Font:";

      let accordionSpan3 = document.createElement("span");
      accordionSpan3.classList.add("accordion-item_span", "bold-item");
      accordionSpan3.textContent = item?.properties?.linea_pedido_font || "-";

      accordionItem3.appendChild(accordionSpanTitle3);
      accordionItem3.appendChild(accordionSpan3);
      accordionColumn1.appendChild(accordionItem3);

      // hided element on desktop
      let accordionItemHide = document.createElement("div");
      accordionItemHide.classList.add("accordion-item", "show-element");
      let accordionSpanTitleHide = document.createElement("span");
      accordionSpanTitleHide.classList.add("accordion-item_title");
      accordionSpanTitleHide.textContent = "Referencia:";

      let accordionSpanHide = document.createElement("span");
      accordionSpanHide.classList.add("accordion-item_span");
      accordionSpanHide.textContent =
        item?.properties?.referencia_del_cliente || "-";

      accordionItemHide.appendChild(accordionSpanTitleHide);
      accordionItemHide.appendChild(accordionSpanHide);
      accordionColumn1.appendChild(accordionItemHide);

      let accordionItemHide2 = document.createElement("div");
      accordionItemHide2.classList.add("accordion-item", "show-element");
      let accordionSpanTitleHide2 = document.createElement("span");
      accordionSpanTitleHide2.classList.add("accordion-item_title");
      accordionSpanTitleHide2.textContent = "Cantidad Pedido:";

      let accordionSpanHide2 = document.createElement("span");
      accordionSpanHide2.classList.add("accordion-item_span");
      accordionSpanHide2.textContent = item?.properties?.cantidad_pedido || "-";

      accordionItemHide2.appendChild(accordionSpanTitleHide2);
      accordionItemHide2.appendChild(accordionSpanHide2);
      accordionColumn1.appendChild(accordionItemHide2);

      let accordionItemHide4 = document.createElement("div");
      accordionItemHide4.classList.add("accordion-item", "show-element");
      let accordionSpanTitleHide4 = document.createElement("span");
      accordionSpanTitleHide4.classList.add("accordion-item_title");
      accordionSpanTitleHide4.textContent = "Cantidad Entregada:";

      let accordionSpanHide4 = document.createElement("span");
      accordionSpanHide4.classList.add("accordion-item_span");
      accordionSpanHide4.textContent =
        item?.properties?.cantidad_entregada || "-";

      accordionItemHide4.appendChild(accordionSpanTitleHide4);
      accordionItemHide4.appendChild(accordionSpanHide4);
      accordionColumn1.appendChild(accordionItemHide4);

      let accordionItemHide1 = document.createElement("div");
      var statusContainer1 = document.createElement("div");
      statusContainer.classList.add("status-container");
      accordionItemHide1.classList.add("accordion-item", "show-element");
      let accordionSpanTitleHide1 = document.createElement("span");
      accordionSpanTitleHide1.classList.add(
        "accordion-item_title",
        "estado-container"
      );
      let accordionSpanHide1 = document.createElement("span");
      accordionSpanHide1.classList.add("accordion-item_span");
      accordionSpanTitleHide1.textContent = "Estado:";
      var spanStatus1 = document.createElement("div");
      accordionSpanHide1.textContent = item?.properties?.estado || "-";
      spanStatus1.classList.add("status");

      statusContainer1.appendChild(spanStatus1);
      statusContainer1.appendChild(accordionSpanHide1);

      accordionItemHide1.appendChild(accordionSpanTitleHide1);
      // accordionItemHide1.appendChild(accordionSpanHide1);
      accordionItemHide1.appendChild(statusContainer1);
      accordionColumn1.appendChild(accordionItemHide1);

      let accordionItemHide3 = document.createElement("div");
      accordionItemHide3.classList.add("accordion-item", "show-element");
      let accordionSpanTitleHide3 = document.createElement("span");
      accordionSpanTitleHide3.classList.add("accordion-item_title");
      accordionSpanTitleHide3.textContent = "Fecha de entrega:";

      let accordionSpanHide3 = document.createElement("span");
      accordionSpanHide3.classList.add("accordion-item_span");
      accordionSpanHide3.textContent = item?.properties?.fecha_entrega || "-";

      accordionItemHide3.appendChild(accordionSpanTitleHide3);
      accordionItemHide3.appendChild(accordionSpanHide3);
      accordionColumn1.appendChild(accordionItemHide3);

      let accordionItem4 = document.createElement("div");
      accordionItem4.classList.add("accordion-item");

      let accordionColumn2 = document.createElement("div");
      accordionColumn2.classList.add("accordion-column");

      let accordionSpanTitle4 = document.createElement("span");
      accordionSpanTitle4.classList.add("accordion-item_title");
      accordionSpanTitle4.textContent = "ID Artículo:";

      let accordionSpan4 = document.createElement("span");
      accordionSpan4.classList.add("accordion-item_span");
      accordionSpan4.textContent = item?.properties?.id_articulo || "-";

      accordionItem4.appendChild(accordionSpanTitle4);
      accordionItem4.appendChild(accordionSpan4);
      accordionColumn2.appendChild(accordionItem4);

      let accordionItem5 = document.createElement("div");
      accordionItem5.classList.add("accordion-item");

      let accordionSpanTitle5 = document.createElement("span");
      accordionSpanTitle5.classList.add("accordion-item_title");
      accordionSpanTitle5.textContent = "Dirección de Envío:";

      let accordionSpan5 = document.createElement("span");
      accordionSpan5.classList.add("accordion-item_span");
      accordionSpan5.textContent = item?.properties?.direccion_envio || "-";

      accordionItem5.appendChild(accordionSpanTitle5);
      accordionItem5.appendChild(accordionSpan5);
      accordionColumn2.appendChild(accordionItem5);

      let accordionButtonContainer = document.createElement("div");
      accordionButtonContainer.classList.add("accordion-button-container");

      // buttons
      let accordionButtonWrapper = document.createElement("div");
      accordionButtonWrapper.classList.add("accordion-button-wrapper");

      var downloadBtn = document.createElement("a");
      let accordinBtn = document.createElement("button");
      accordinBtn.classList.add("button-accordin");
      accordinBtn.textContent = "Descargar";
      downloadBtn.href = item?.documento || "#";
      downloadBtn.download = "download.pdf"; // Set the filename here
      downloadBtn.target = "_blank";
      downloadBtn.appendChild(accordinBtn);

      accordionButtonWrapper.appendChild(downloadBtn);

      let accordionButtonWrapper2 = document.createElement("div");
      accordionButtonWrapper2.classList.add("accordion-button-wrapper");

      let solicitarBtn = document.createElement("button");
      solicitarBtn.setAttribute("class", "popup-modal");

      solicitarBtn.textContent = "Solicitar nueva fecha de entrega";
      accordionButtonWrapper2.appendChild(solicitarBtn);

      let accordionButtonWrapper3 = document.createElement("div");
      accordionButtonWrapper3.classList.add("accordion-button-wrapper");

      let hacerBtn = document.createElement("button");
      hacerBtn.classList.add("button-accordin");
      hacerBtn.textContent = "Hacer Seguimiento De Pedido";

      solicitarBtn.addEventListener("click", () =>
        openModal(
          item.id,
          item.properties.referencia_del_cliente,
          item.properties.fecha_entrega,
          item.properties.id_pedido_font,
          item.properties.id_cliente_font,
          item.companyid
        )
      );
      hacerBtn.addEventListener("click", () => TrackopenModal(item.id));

      window.onclick = function (event) {
        if (event.target == overlay) {
          closeModal();
        }
        if (event.target == Trackoverlay) {
          TrackcloseModal();
        }
      };
      accordionButtonWrapper3.appendChild(hacerBtn);

      accordionButtonContainer.appendChild(accordionButtonWrapper);
      accordionButtonContainer.appendChild(accordionButtonWrapper2);
      accordionButtonContainer.appendChild(accordionButtonWrapper3);

      accordionBtn.addEventListener("click", function () {
        // Get the hide elements within the clicked accordion
        let hideElements =
          this.parentElement.parentElement.querySelectorAll(".hide-element");

        // Toggle the visibility of hide elements
        hideElements.forEach(function (element) {
          element.classList.toggle("hide-elements");
        });

        accordionContent.classList.toggle("active-accordion");
        itemDiv.classList.toggle("accordion-header");
        var currentSrc = img.src;
        var targetSrc =
          currentSrc === originalImgSrc ? newImgSrc : originalImgSrc;
        img.src = targetSrc;
      });

      itemDiv.appendChild(singleItemDiv7);

      accordionContentWrapper.appendChild(accordionColumn1);
      accordionContentWrapper.appendChild(accordionColumn2);

      accordionContent.appendChild(accordionContentWrapper);
      accordionContent.appendChild(accordionButtonContainer);

      combaineConatiner.appendChild(itemDiv);
      combaineConatiner.appendChild(accordionContent);
      childDiv.appendChild(combaineConatiner);
      parentDiv.appendChild(childDiv);
    });

    var paginationDiv = document.createElement("div");
    paginationDiv.classList.add("pagination");
    paginationDiv.classList.add("padidos-pagination");
    let paginationContainer = document.querySelector(".pagination-container");
    var arrowImg = document.createElement("img");
    arrowImg.src =
      "https://9149224.fs1.hubspotusercontent-na1.net/hubfs/9149224/Area%20privada%20-%20FONT/Vector%208-1.svg";
    arrowImg.alt = "";

    function goToPage(page) {
      currentPage = page;
      arrowImg.style.opacity = 1;
      arrowImg.style.pointerEvents = "auto";

      updateTable(data); // Update the table
      // Remove existing pagination container if it exists
      const existingPaginationDiv = document.querySelector(".pagination");
      if (existingPaginationDiv) {
        existingPaginationDiv.remove();
      }
      renderPagination(); // Re-render pagination
    }

    // Right arrow onclick event handler
    arrowImg.onclick = function () {
      if (currentPage < totalPages) {
        currentPage++;
        goToPage(currentPage);
      }
    };

    let ellipsis = "...";
    // Function to render pagination
    function renderPagination() {
      paginationDiv.innerHTML = ""; // Clear existing pagination buttons

      const maxVisibleButtons = 4;

      // Calculate start and end page numbers
      let startPage = 1;
      let endPage = Math.min(totalPages, maxVisibleButtons);

      if (totalPages > maxVisibleButtons) {
        if (currentPage >= maxVisibleButtons - 1) {
          startPage = currentPage - 1;
          endPage = Math.min(currentPage + 1, totalPages);
          if (endPage === totalPages) {
            startPage = totalPages - (maxVisibleButtons - 1);
          }
          paginationDiv.appendChild(createPageButton(1)); // Add first page button
          paginationDiv.appendChild(createPageButton(ellipsis)); // Add ellipsis
        } else {
          endPage = maxVisibleButtons;
        }
      }

      if (currentPage === totalPages) {
        arrowImg.style.opacity = 0.5;
        arrowImg.style.pointerEvents = "none";
      }

      for (let i = startPage; i <= endPage; i++) {
        paginationDiv.appendChild(createPageButton(i));
      }

      if (totalPages > maxVisibleButtons && currentPage < totalPages - 2) {
        paginationDiv.appendChild(createPageButton(ellipsis)); // Add ellipsis
        paginationDiv.appendChild(createPageButton(totalPages)); // Add last page button
      }
      if (data.length > 1) {
        paginationDiv.appendChild(arrowImg); // Append the right arrow
      }
      // parentDiv.appendChild(paginationDiv); // Append paginationDiv to the document
      paginationContainer.innerHTML = ""; // Clear the existing pagination
      paginationContainer.appendChild(paginationDiv);
    }

    // Function to create page button
    function createPageButton(page) {
      const pageButton = document.createElement("button");
      pageButton.textContent = page === ellipsis ? ellipsis : page;
      if (page === currentPage) {
        pageButton.classList.add("active");
      }
      if (page !== ellipsis) {
        pageButton.onclick = function () {
          goToPage(page);
        };
      }
      return pageButton;
    }

    var totalPages = Math.ceil(data.length / itemsPerPage);

    renderPagination(); // Initial rendering of pagination
  } catch (error) {
    console.error("Error updating table:", error);
  }
}

// popup modal
var modal = document.getElementById("modal");
var modalTrack = document.getElementById("track-modal");
let overlay = document.querySelector(".pedidos-popup-container");
let date = document.getElementById("date");
let Trackoverlay = document.querySelector(".pedidos-track-popup-container");

var body = document.body;
body.style.overflow = "auto";
// Function to open modal and overlay
let dateInput = document.querySelector(".date-attribute");
let track = document.querySelector(".track");
var checkbox = document.getElementById("checkbox");
let dateValue;

function openModal(id, referencia, date, id_font, id_client, comId) {
  let description = `Referencia:${referencia},\n ID Pedido Font: ${id_font}, \n ID Cliente Font: ${id_client},\n Fecha de entrega anterior: ${date}`;
  dateInput.setAttribute("data-attribute", id);
  dateInput.setAttribute("data-description", description);
  dateInput.setAttribute("data-comId", comId);
  // dateInput.setAttribute("data-comId", contactId);/

  modal.style.display = "flex";
  overlay.style.display = "block";
  body.style.overflow = "hidden";
}

// Function to close modal and overlay
function closeModal() {
  modal.style.display = "none";
  overlay.style.display = "none";
  body.style.overflow = "auto";
  date.value = "";
}

// modal for track popup
function TrackopenModal(id) {
  track.setAttribute("data-attribute", id);
  body.style.overflow = "hidden";
  modalTrack.style.display = "flex";
  Trackoverlay.style.display = "block";
}

// Function to close modal and overlay
function TrackcloseModal() {
  body.style.overflow = "auto";

  // modal for track popup
  modalTrack.style.display = "none";
  Trackoverlay.style.display = "none";
  checkbox.checked = false;
}

const getCompanyId = async (id) => {
  try {
    const response = await fetch(
      `https://fontpackaging.com/_hcms/api/pedidos_companyId?portalid=9149224&id=${id}`
    );
    const data = await response?.json();
    return data;
  } catch (err) {
    console.error(err);
    return null;
  }
};

// update Tracking field functionality
const trackUpdate = (id, checked, contactId) => {
  fetch(
    `https://fontpackaging.com/_hcms/api/track-data?portalid=9149224&id=${id}`,
    {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        propertyValue: checked,
        contactId: contactId,
      }),
    }
  )
    .then((response) => response.json())
    .then(() => {
      TrackcloseModal();
      trackUpdateAss(id, contactId);
    })
    .catch((err) => {
      console.error(err);
    });
};

const trackUpdateAss = (id, contactId) => {
  console.log(id, "it's ID", contactId, ":ItsContact");
  fetch("https://fontpackaging.com/_hcms/api/track_pedidos?portalid=9149224", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      contactId: contactId,
      id: id,
    }),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      return response.text();
    })
    .then((result) => {
      if (result === "") {
        console.log("Success with no content to display.");
      } else {
        console.log("Response:", result);
      }
    })
    .catch((err) => {
      console.error(err);
    });
};

// let trackFormId = document.getElementById("track-modal");
// trackFormId.addEventListener("submit", (e) => {
//   let dateAtt = track.getAttribute("data-attribute");
//   var isChecked = checkbox.checked;
//   e.preventDefault();
//   trackUpdate(dateAtt, isChecked,contactId);
// });

let trackFormId = document.getElementById("track-modal");
trackFormId.addEventListener("submit", (e) => {
  e.preventDefault();
  let dateAtt = track.getAttribute("data-attribute");
  var isChecked = checkbox.checked;
  if (typeof contactId !== "undefined") {
    console.log(contactId);
    trackUpdate(dateAtt, isChecked, contactId);
  } else {
    console.error("contactId is not defined");
  }
});

// handled previous date cant be selected
var today = new Date();
var dd = String(today.getDate()).padStart(2, "0");
var mm = String(today.getMonth() + 1).padStart(2, "0"); // January is 0!
var yyyy = today.getFullYear();

today = yyyy + "-" + mm + "-" + dd;
document.getElementById("date").min = today;
// update date field functionality
const updatedDate = (id, des, comId, hubspot_owner_id) => {
  dateValue = date.value;
  const updatedDes = `${des}\nFecha de entrega: ${dateValue}`;
  console.log(updatedDes);
  fetch(
    `https://fontpackaging.com/_hcms/api/date-update?portalid=9149224&id=${id}`,
    {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        propertyName: "fecha_entrega",
        propertyValue: dateValue,
      }),
    }
  )
    .then((response) => response.json())
    .then(() => {
      ticketCreation(id, updatedDes, comId, hubspot_owner_id);
      closeModal();
    })
    .catch((err) => {
      console.error(err);
    });
};

let hubspot_owner_id;
//Email Search for Ticket
// const emailSearch = (gestorEmail) => {
//   fetch(`https://fontpackaging.com/_hcms/api/email_pedidos?portalid=9149224`, {
//     method: "POST",
//     headers: {
//       "Content-Type": "application/json",
//     },
//     body: JSON.stringify({
//       gestorEmail: gestorEmail,
//     }),
//   })
//     .then((response) => response.json())
//     .then((result) => {
//       let data = JSON.parse(result);
//       const id = data.id;
//       console.log("id:", id);
//       hubspot_owner_id = id;
//     })
//     .catch((err) => {
//       console.error(err);
//     });
// };

const emailSearch = (gestorEmail) => {
  console.log(gestorEmail)
  return new Promise((resolve, reject) => {
    fetch(
      `https://fontpackaging.com/_hcms/api/email_pedidos?portalid=9149224`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          gestorEmail: gestorEmail,
        }),
      }
    )
      .then((response) => response.json())
        .then((result) => {
          let data = JSON.parse(result);
          const id = result.id;
          console.log(data.id);
        hubspot_owner_id = data.id;
        resolve(id);
        return hubspot_owner_id;
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
};

// ticket Creation
const ticketCreation = (id, des, comId, emailSearch) => {
  fetch(
    `https://fontpackaging.com/_hcms/api/ticket-generation?portalid=9149224`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: id,
        comId: comId,
        des: des,
        hubspot_owner_id: emailSearch,
        contactId:contactId,
      }),
    }
  )
    .then((response) => response.text())
    .then(() => {
      window.location.reload();
    })
    .catch((err) => {
      console.error(err);
    });
};

let formId = document.getElementById("update-date");

formId.addEventListener("submit", (e) => {
  e.preventDefault();
  let dateAtt = dateInput.getAttribute("data-attribute");
  let dataDes = dateInput.getAttribute("data-description");
  let comId = dateInput.getAttribute("data-comid");

  Promise.all([...gestorEmail].map((email) => emailSearch(email)))
    .then(() => {
      updatedDate(dateAtt, dataDes, comId, hubspot_owner_id);
    })
    .catch((err) => {
      console.error(err);
    });
});

// data update function
const updatedObject = async (id) => {
  return fetch(
    `https://fontpackaging.com/_hcms/api/server_render_pedidos?portalid=9149224&id=${id}`
  )
    .then((response) => response.json())
    .catch((err) => {
      console.error(err);
      return null;
    });
};

// Fetch initial data
let companyId = [];
let contactId;
let gestorEmail = new Set();
const customObject = () => {
  showLoader();
  fetch(`https://fontpackaging.com/_hcms/api/pedidos?portalid=9149224`)
    .then((response) => response.json())
    .then((data) => {
      if (Array.isArray(data) && Array.isArray(data[0])) {
        data = data.flat();
      }
      companyId = data.companyIds;
      contactId = data.contactId;

      data.allData.flatMap((item) =>
        item.results.map((result) =>
          gestorEmail.add(result.properties.gestor_email)
        )
      );
      // gestorEmail.forEach((email) => {
      //   emailSearch(email);
      // });
      const fetchDataForDocuments = data.allData.flatMap((item) =>
        item.results.map(async (result) => {
          let companyIdData;
          try {
            companyIdData = await getCompanyId(result?.id);
          } catch (error) {
            console.error(error);
          }
          // If the result has a 'properties.documento', fetch its details
          if (
            result.properties &&
            companyIdData?.results[0]?.id != 0 &&
            companyIdData?.results[0] &&
            result.properties.documento &&
            result.properties.documento != 0
          ) {
            const documentoId = result.properties.documento;

            return fetch(
              `https://fontpackaging.com/_hcms/api/pedidos-download?portalid=9149224&id=${documentoId}`,
              {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
              }
            )
              .then((response) => response.json())
              .then((data) => {
                hideLoader();
                const parsedData = JSON.parse(data);
                return {
                  ...result,
                  documento: parsedData.url,
                  companyid: companyIdData?.results[0]?.id,
                }; // Merge the document URL into the result
              })
              .catch((error) =>
                console.error("Error fetching document details:", error)
              );
          } else {
            return Promise.resolve({
              ...result,
              companyid: companyIdData?.results[0]?.id,
            }); // Return the result as is if no documento
          }
        })
      );
      Promise.all(fetchDataForDocuments).then((allResults) => {
        updateTable(allResults);
      });
    })
    .catch((error) => console.error("Error:", error));
};

customObject();

// Search
document.getElementById("searchBar").addEventListener("input", function () {
  let searchValue = this.value.trim(); // Get the search value and trim any extra whitespace

  // Reset the current page to 1 on a new search
  currentPage = 1; // Get the search value // Get the search value
  if (searchValue.length === 0) {
    customObject();
  } else {
    // List of property names to search
    let propertyNames = [
      // "empresa",
      "id_pedido_cliente",
      "referencia_del_cliente",
      // "cantidad_pedido",
      // "fecha_entrega",
      // "cantidad_entregada",
      // "estado",
      // "id_cliente_font",
      // "id_pedido_font",
      // "linea_pedido_font",
      // "id_articulo",
      // "direccion_envio",
    ];
    // Fetch data for each property name
    propertyNames.forEach((propertyName) => {
      fetch(
        "https://fontpackaging.com/_hcms/api/search_pedidos?portalid=9149224",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            propertyName: propertyName,
            propertyValue: searchValue,
            companyId,
          }),
        }
      )
        .then((response) => response.json())
        .then((data) => {
          const idsToFetch = data[0]?.results.map((item) => item.id) || [];

          let fetchPromises = idsToFetch.map((id) => updatedObject(id));

          Promise.all(fetchPromises)
            .then((allData) => {
              updateTable(allData);
            })
            .catch((error) => {
              console.error(error);
            });
        })
        .catch((error) => {
          console.log(error);
        });
    });
  }
});

// filter function
document.getElementById("dropdown").addEventListener("change", function () {
  let selectedValue = this.value; // Get the selected value
  let filterValue = "";
  if (selectedValue === "font") {
    filterValue = "CF";
  } else if (selectedValue === "troquel") {
    filterValue = "TS";
  }

  if (filterValue) {
    fetch(
      "https://fontpackaging.com/_hcms/api/filter_pedidos?portalid=9149224",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },

        body: JSON.stringify({
          propertyName: "empresa",
          propertyValue: filterValue,
          companyId,
        }),
      }
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.results.length === 0) {
          updateTable([]);
        } else {
          const idsToFetch = data.results.map((item) => item.id);

          let fetchPromises = idsToFetch.map((id) => updatedObject(id));

          Promise.all(fetchPromises)
            .then((allData) => {
              updateTable(allData);
              console.log(allData, "+++++++++");
            })
            .catch((error) => {
              console.error(error);
            });
        }
      })

      .catch((error) => {
        console.error(error);
      });
  } else {
    customObject();
  }
});

