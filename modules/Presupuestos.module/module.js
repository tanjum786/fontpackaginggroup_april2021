var currentPage = 1;
var itemsPerPage = 5;

const loader = document.getElementById("loader");

const showLoader = () => {
  loader.classList.remove("loader-hidden");
};

const hideLoader = () => {
  loader.classList.add("loader-hidden");
};

let paginationContainer = document.querySelector(".pagination-container");
// Function to update the table
function updateTable(data) {
  try {
    var parentDiv = document.getElementById("data-table");
    // var parentDiv = document.getElementById("data-table");

    parentDiv.innerHTML = "";
    var start = (currentPage - 1) * itemsPerPage;
    var end = start + itemsPerPage;
    var pageData = data.slice(start, end);

    pageData.forEach((item) => {
      // Create a div to contain the data for each item
      var itemDiv = document.createElement("div");
      itemDiv.classList.add("data_container");

      var combinedDiv = document.createElement("div");
      combinedDiv.classList.add("table-container");

      // Create and append spans for each property
      var spanEmpresa = document.createElement("span");
      spanEmpresa.textContent = item?.properties?.empresa || "-";
      itemDiv.appendChild(spanEmpresa);

      var spanIdPresupuesto = document.createElement("span");
      spanIdPresupuesto.textContent = item?.properties?.id_presupuesto || "-";
      spanIdPresupuesto.style.fontWeight = "600";
      itemDiv.appendChild(spanIdPresupuesto);

      var spanTitulo = document.createElement("span");
      spanTitulo.textContent = item?.properties?.titulo || "-";
      itemDiv.appendChild(spanTitulo);

      var spanFecha = document.createElement("span");
      spanFecha.textContent = item?.properties?.fecha || "-";
      itemDiv.appendChild(spanFecha);

      var spanId = document.createElement("span");
      spanId.textContent = item?.properties?.id_cliente_font || "-";
      itemDiv.appendChild(spanId);

      combinedDiv.appendChild(itemDiv);

      var defDiv = document.createElement("div");
      defDiv.classList.add("download-button_container");

      var downloadImageDiv = document.createElement("div");
      downloadImageDiv.classList.add("download-image");

      var img = document.createElement("img");
      var downloadBtn = document.createElement("a");
      img.src =
        "https://9149224.fs1.hubspotusercontent-na1.net/hubfs/9149224/Area%20privada%20-%20FONT/Vector.svg";
      img.alt = "download-image";

      // Add the documento link if available
      downloadBtn.href = item?.documento || "#";
      downloadBtn.download = item?.properties?.titulo || "download.pdf"; // Set the filename here
      downloadBtn.target = "_blank";

      downloadBtn.appendChild(img);
      downloadImageDiv.appendChild(downloadBtn);

      defDiv.appendChild(downloadImageDiv);
      combinedDiv.appendChild(defDiv);

      parentDiv.appendChild(combinedDiv);
    });

    var paginationDiv = document.createElement("div");
    paginationDiv.classList.add("pagination");
    var arrowImg = document.createElement("img");
    arrowImg.src =
      "https://9149224.fs1.hubspotusercontent-na1.net/hubfs/9149224/Area%20privada%20-%20FONT/Vector%208-1.svg";
    arrowImg.alt = "";

    function goToPage(page) {
      currentPage = page;
      updateTable(data);
      arrowImg.style.opacity = 1;
      arrowImg.style.pointerEvents = "auto";
      const existingPaginationDiv = document.querySelector(".pagination");
      if (existingPaginationDiv) {
        existingPaginationDiv.remove();
      }
      renderPagination(); // Re-render pagination
    }

    // Right arrow onclick event handler
    arrowImg.onclick = function() {
      if (currentPage < totalPages) {
        currentPage++;
        goToPage(currentPage);
      }
    };

    let ellipsis = "...";
    // Function to render pagination
    function renderPagination() {
      paginationDiv.innerHTML = ""; // Clear existing pagination buttons

      const maxVisibleButtons = 4;

      // Calculate start and end page numbers
      let startPage = 1;
      let endPage = Math.min(totalPages, maxVisibleButtons);

      if (totalPages > maxVisibleButtons) {
        if (currentPage >= maxVisibleButtons - 1) {
          startPage = currentPage - 1;
          endPage = Math.min(currentPage + 1, totalPages);
          if (endPage === totalPages) {
            startPage = totalPages - (maxVisibleButtons - 1);
          }
          paginationDiv.appendChild(createPageButton(1)); // Add first page button
          paginationDiv.appendChild(createPageButton(ellipsis)); // Add ellipsis
        } else {
          endPage = maxVisibleButtons;
        }
      }

      if (currentPage === totalPages) {
        arrowImg.style.opacity = 0.5;
        arrowImg.style.pointerEvents = "none";
      }

      for (let i = startPage; i <= endPage; i++) {
        paginationDiv.appendChild(createPageButton(i));
      }

      if (totalPages > maxVisibleButtons && currentPage < totalPages - 2) {
        paginationDiv.appendChild(createPageButton(ellipsis)); // Add ellipsis
        paginationDiv.appendChild(createPageButton(totalPages)); // Add last page button
      }
      // if (data.length > 0) {
      //   paginationDiv.appendChild(arrowImg); // Append the right arrow
      // }
      // parentDiv.appendChild(paginationDiv); // Append paginationDiv to the document
      if (data.length > 1) {
        paginationDiv.appendChild(arrowImg); // Append the right arrow
      }
      paginationContainer.innerHTML = ""; // Clear the existing pagination
      paginationContainer.appendChild(paginationDiv); // Append the new pagination to the paginationContainer
    }

    // Function to create page button
    function createPageButton(page) {
      const pageButton = document.createElement("button");
      pageButton.textContent = page === ellipsis ? ellipsis : page;
      if (page === currentPage) {
        pageButton.classList.add("active");
      }
      if (page !== ellipsis) {
        pageButton.onclick = function() {
          goToPage(page);
        };
      }
      return pageButton;
    }

    var totalPages = Math.ceil(data.length / itemsPerPage);

    renderPagination(); // Initial rendering of pagination
  } catch (error) {
    console.error("Error updating table:", error);
  }
}

const updatedObject = async (id) => {
  return fetch(
    `https://fontpackaging.com/_hcms/api/server_render?portalid=9149224&id=${id}`
  )
    .then((response) => response.json())
    .catch((err) => {
      console.error(err);
      return null;
    });
};

// Fetch initial data

let companyId = [];

const customObject = () => {
  showLoader();
  fetch("https://fontpackaging.com/_hcms/api/presupuestos?portalid=9149224")
    .then((response) => response.json())
    .then((data) => {
      // Flatten data if nested
      if (Array.isArray(data) && Array.isArray(data[0])) {
        data = data.flat();
      }
      companyId = data.companyIds;
      const fetchDataForDocuments = data.allData.flatMap((item) =>
        item.results.map((result) => {
          // If the result has a 'properties.documento', fetch its details
          if (
            result.properties &&
            result.properties.documento &&
            result.properties.documento != 0
          ) {
            const documentoId = result.properties.documento;
            return fetch(
              `https://fontpackaging.com/_hcms/api/download?portalid=9149224&id=${documentoId}`,
              {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
              }
            )
              .then((response) => response.json())
              .then((data) => {
                const parsedData = JSON.parse(data);
                hideLoader();

                return { ...result, documento: parsedData.url }; // Merge the document URL into the result
              })
              .catch((error) =>
                console.error("Error fetching document details:", error)
              );
          } else {
            return Promise.resolve(result); // Return the result as is if no documento
          }
        })
      );

      // Wait for all document fetches to complete
      Promise.all(fetchDataForDocuments).then((allResults) => {
        updateTable(allResults);
      });
    })
    .catch((error) => {
      console.error("Error:", error);
      hideLoader();
    });
};

customObject();

// Search
document.getElementById("searchBar").addEventListener("input", function() {
  let searchValue = this.value.trim(); // Get the search value and trim any extra whitespace

  // Reset the current page to 1 on a new search
  currentPage = 1; // Get the search value
  if (searchValue.length === 0) {
    customObject();
  } else {
    // List of property names to search
    // let propertyNames = ["empresa", "id_presupuesto", "titulo","id_cliente_font"];
    let propertyNames = ["titulo","id_presupuesto"];
    

    // Fetch data for each property name
    propertyNames.forEach((propertyName) => {
      fetch("https://fontpackaging.com/_hcms/api/search?portalid=9149224", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          propertyName: propertyName,
          propertyValue: searchValue,
          companyId,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
          const idsToFetch = data[0]?.results.map((item) => item.id) || [];

          let fetchPromises = idsToFetch.map((id) => updatedObject(id));

          Promise.all(fetchPromises)
            .then((allData) => {
              updateTable(allData);
            })
            .catch((error) => {
              console.error(error);
            });
        })
        .catch((error) => {
          console.log(error);
        });
    });
  }
});

//Filter
// Update the dropdown change event listener
document.getElementById("dropdown").addEventListener("change", function() {
  let selectedValue = this.value; // Get the selected value
  let filterValue = "";
  console.log(selectedValue, "selcetdValue");
  if (selectedValue === "font") {
    filterValue = "CF";
  } else if (selectedValue === "troquel") {
    filterValue = "TS";
  }

  if (filterValue) {
    fetch("https://fontpackaging.com/_hcms/api/filter?portalid=9149224", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        propertyName: "empresa",
        propertyValue: filterValue,
        companyId,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.results.length === 0) {
          updateTable([]);
        } else {
          const idsToFetch = data.results.map((item) => item.id);

          let fetchPromises = idsToFetch.map((id) => updatedObject(id));

          Promise.all(fetchPromises)
            .then((allData) => {
              updateTable(allData.filter((item) => item));
            })
            .catch((error) => {
              console.error(error);
            });
        }
      })

      .catch((error) => {
        console.error(error);
      });
  } else {
    customObject();
  }
});
