function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}
 
// Search
$('#search-productos').keyup(delay(function(e){
  
  //var textos = $(this).val();

  var query1 = buildQuery();
  getProductos(query1);
 
}, 500));

function getProductos(query) {
  
  if (query.includes("+")) {
    query = query.replace("+", "%2b");
  }
  
  $.ajax({
      type: 'GET',
      url: 'https://api.hubapi.com/cms/v3/hubdb/tables/5307489/rows',
      data: 'portalId=9149224' + query,
    }).done(function (respuesta) {
    //var resultados = JSON.stringify(respuesta.results[0].score);
    //console.log(respuesta.results);
    //console.log(respuesta);
    $("#listado-productos").hide();
    
    //respuesta.results.sort((a,b) => (b.values.orden > a.values.orden) ? 1 : ((a.values.orden > b.values.orden) ? -1 : 0));
    //var selectedCategoria = $('#categorias').children("option:selected").val();
    
    //if(selectedCategoria) {
      //console.log("Categoria selected");
      //var arraySubcategorias = [];
    //}
    
    if(respuesta.results.length > 0) {
      $("#search-results").html("");
      $("#nothing-found").hide();
      $('#search-results').show();
      //console.log(respuesta.results);
       for(var i=0; i<respuesta.results.length; i++){
       
         var lang = document.documentElement.lang;
         var name = respuesta.results[i].values.name;
         var url = respuesta.results[i].path;
         var image = respuesta.results[i].values.imagen;
         var imageUrl = "https://f.hubspotusercontent20.net/hubfs/9149224/raw_assets/public/INSUPACKFontPackagingGroup_April2021/images/PlaceholderProductos.png";
         //var subcategorias = respuesta.results[i].values.subcategoria;
         var slug_products = "productos-por-categoria";
         var slug_lang = "";
         
         //if(selectedCategoria && subcategorias !== null) {
           //subcategorias.forEach(subcategoria => arraySubcategorias.push(subcategoria.name));
         //}
         
         if(lang == "en") {
           slug_lang = "/en";
           slug_products = "products-by-category";
           name = respuesta.results[i].values.nombre_en;
         }
         
         if(lang == "fr") {
           slug_lang = "/fr";
           slug_products = "produits-par-categorie";
           name = respuesta.results[i].values.nombre_fr;
         }
         
         if(lang == "ca") {
           slug_lang = "/ca";
           slug_products = "productes-per-categoria";
           name = respuesta.results[i].values.nombre_ca;
         }
         

         if(image) {
           imageUrl = image.url;
         }
         
         
         var contenido = '<div class="item-producto">'
         + '<div class="box-producto">'
         + '<a href="'+ slug_lang +'/'+ slug_products +'/'+ url +'" class="box">'
         + '<img src="'+ imageUrl +'" alt="'+ name + '">'
         + '<h4 class="mt10"><strong>'+ name +'</strong></h4>'
         + '</a>'
         + '</div></div>';
         
         $('#search-results').append(contenido);
      }
    } else {
      $('#search-results').hide();
      $("#nothing-found").show();
    }
    
    //if(arraySubcategorias) {
      //console.log(unique(arraySubcategorias));
    //}
    
    
  }).fail(function (msg) {
    console.log('FAIL');
  });
}

function buildQuery() {
  var lang = document.documentElement.lang;
  var querySearch = "&name__icontains=" + $('#search-productos').val();
  
  if(lang == "es") {
    var querySearch = "&name__icontains=" + $('#search-productos').val();
  }

  if(lang == "en") {
    var querySearch = "&nombre_en__icontains=" + $('#search-productos').val();
  }

  if(lang == "fr") {
    var querySearch = "&nombre_fr__icontains=" + $('#search-productos').val();
  }

  if(lang == "ca") {
    var querySearch = "&nombre_ca__icontains=" + $('#search-productos').val();
  }
  
  var categoriaSelect = $('#categorias');
  var subcategoriaSelect = $('#subcategorias');
  var selectedCategoria = categoriaSelect.children("option:selected").val();
  var selectedSubcategoria = subcategoriaSelect.children("option:selected").val();
  var queryCategoria = "";
  var querySubcategoria = "";
  
  if(selectedCategoria) {
    
    if(lang == "es") {
      var queryCategoria= "&categoria__icontains=" + selectedCategoria;
    }
    
    if(lang == "en") {
      var queryCategoria= "&categoria_en__icontains=" + selectedCategoria;
    }
    
    if(lang == "fr") {
      var queryCategoria= "&categoria_fr__icontains=" + selectedCategoria;
    }
    
    if(lang == "ca") {
      var queryCategoria= "&categoria_ca__icontains=" + selectedCategoria;
    }
    
  }
  
  if(selectedSubcategoria) {
    var lang = document.documentElement.lang;
    if(lang == "es") {
      var querySubcategoria = "&subcategoria__icontains=" + selectedSubcategoria;
    }
    if(lang == "en") {
      var querySubcategoria = "&subcategoria_en__icontains=" + selectedSubcategoria;
    }
    if(lang == "fr") {
      var querySubcategoria = "&subcategoria_fr__icontains=" + selectedSubcategoria;
    }
    if(lang == "ca") {
      var querySubcategoria = "&subcategoria_ca__icontains=" + selectedSubcategoria;
    }
    
  }
  
  if (selectedCategoria != "" && selectedSubcategoria == "") {
    var querySubcategoria = "";
  }
  
  var queryFilters = queryCategoria + querySubcategoria;
  var query = queryFilters ;
  
  if ($('#search-productos').val().length > 2) {
    query = querySearch + queryFilters;
  }
  
  return query + "&sort=random()";
}

$("#categorias").change(function(){
  
  var lang = document.documentElement.lang;
  var categoriaSelect = $('#categorias');
  var subcategoriaSelect = $('#subcategorias');
  var selectedCategoria = categoriaSelect.children("option:selected").val();
  var listaDeSubcategorias = [];
  
  var todasLasSubcategorias = [
      'Cajas de Cartón', 
      'Cajas para Botellas', 
      'Cajas para Mercancías Peligrosas', 
      'Carretes de cartón', 
      'Cajas con recubrimientos especiales', 
      'Cajas para Lotes', 
      'Cajas para ropa y textil', 
      'Packaging offset', 
      'Cajas para ecommerce',
      'Gran Embalaje Industrial',
      'Octabines de sólidos',
      'Contenedores para líquidos',
      'Kits de Embalaje',
      'Cajas Reutilizables',
      'Contenedores Reutilizables',
      'Acondicionadores de embalaje',
      'Cajas Isotérmicas',
      'ECO+',
      'Contenedores Isotérmicos',
      'Acumuladores',
      'Material Anticorrosión',
      'Relleno y Protección',
      'Celulosa Moldeada',
      'Cantoneras y Tubos',
      'Cintas adhesivas de papel',
      'CLICKpal',
      'CLICKbox',
      'CLICKair'
      ];
  
  var todasLasSubcategorias_EN = [
      'Cardboard Boxes', 
      'Bottle Cases', 
      'Dangerous Goods Boxes', 
      'Cardboard Reels', 
      'Boxes with special coatings', 
      'Boxes for batches', 
      'Clothing and textile boxes', 
      'Offset packaging', 
      'Ecommerce Boxes',
      'Large Industrial Packaging',
      'Octabins for solids',
      'Containers for liquids',
      'Packaging kits',
      'Reusable boxes',
      'Reusable containers',
      'Packaging conditioners',
      'Isothermal Boxes',
      'ECO+',
      'Isothermal Containers',
      'EAcumulators',
      'Anticorrosion Material',
      'Filling and Protection',
      'Molded Cellulose',
      'Corner pieces and tubes',
      'Paper adhesive tapes',
      'CLICKpal',
      'CLICKbox',
      'CLICKair'
      ];
  
  var todasLasSubcategorias_FR = [
      'Boîtes en carton', 
      'Casiers à bouteilles', 
      'Boîtes pour marchandises dangereuses', 
      'Enrouleurs en carton', 
      'Boîtes avec des revêtements spéciaux', 
      'Boîtes pour les lots', 
      'Boîtes à vêtements et à textiles', 
      'Emballage offset', 
      'Boîtes de commerce électronique',
      'Grands emballages industriels',
      'Octabins pour solides',
      'Récipients pour liquides',
      'Kits d\'emballage',
      'Boîtes réutilisables',
      'Conteneurs réutilisables',
      'Conditionneurs d\'emballage',
      'Boîtes isothermes',
      'ECO+',
      'Conteneurs isothermes',
      'Accumulateurs',
      'Matériau anticorrosion',
      'Remplissage et protection',
      'Fibre moulée',
      'Protections d\'angle et protections d\'angle',
      'Rubans adhésifs en papier',
      'CLICKpal',
      'CLICKbox',
      'CLICKair'
      ];
  
  var todasLasSubcategorias_CA = [
      'Caixes de cartró', 
      'Caixes per ampolles', 
      'Caixes per a Mercaderies Perilloses', 
      'Carrets de cartró', 
      'Caixes cpn recobriments especials', 
      'Caixes per a Lots', 
      'Caixes per a roba i tèxtil', 
      'Packaging offset', 
      'Caixes per a ecommerce',
      'Gran embalatge industrial',
      'Octabins per a sòlids',
      'Contenidors per a líquids',
      'Kits d\'embalatge',
      'Caixes reutilitzables',
      'Contenidors reutilitzables',
      'Condicionadors d\'embalatge',
      'Caixes Isotèrmiques',
      'ECO+',
      'Contenidors Isotèrmics',
      'Acumuladors',
      'Material Anticorrosió',
      'Farciment i protecció',
      'Cel·lulosa Emmotllada',
      'Cantoneres i tubs',
      'Cintas adhesivas de papel',
      'CLICKpal',
      'CLICKbox',
      'CLICKair'
      ];
  
  if(lang == "es") {
    if(selectedCategoria == 'Embalaje de Consumo') {
      listaDeSubcategorias = ['Cajas de Cartón', 'Cajas para Botellas', 'Cajas para Mercancías Peligrosas', 'Carretes de cartón', 'Cajas con recubrimientos especiales', 'Cajas para Lotes', 'Cajas para ropa y textil', 'Packaging offset', 'Cajas para ecommerce'];
    }
    if(selectedCategoria == 'Embalaje Industrial') {
      listaDeSubcategorias = ['Gran Embalaje Industrial', 'Octabines de sólidos', 'Contenedores para líquidos', 'Kits de Embalaje'];
    }
    if(selectedCategoria == 'Embalaje de Plástico') {
      listaDeSubcategorias = ['Cajas Reutilizables', 'Contenedores Reutilizables', 'Acondicionadores de embalaje'];
    }
    if(selectedCategoria == 'Embalaje Isotérmico') {
      listaDeSubcategorias = ['Cajas Isotérmicas', 'ECO+', 'Contenedores Isotérmicos', 'Acumuladores'];
    }
    if(selectedCategoria == 'Embalaje de Protección') {
      listaDeSubcategorias = ['Material Anticorrosión', 'Relleno y Protección', 'Celulosa Moldeada', 'Cantoneras y Tubos', 'Cintas adhesivas de papel'];
    }
    if(selectedCategoria == 'Palets de Cartón') {
      listaDeSubcategorias = ['CLICKpal', 'CLICKbox', 'CLICKair'];
    }
    if(selectedCategoria == 'Cartón Ondulado') {
      listaDeSubcategorias = [];
    }
    if(selectedCategoria == '') {
      listaDeSubcategorias = todasLasSubcategorias;
    }

    subcategoriaSelect.empty();
    subcategoriaSelect.append($("<option></option>").attr("value", "").text("Todas las subcategorias")); 
  }
  
  if(lang == "en") { 
    if(selectedCategoria == 'Consumer Packaging') {
      listaDeSubcategorias = ['Cardboard Boxes', 'Bottle Cases', 'Dangerous Goods Boxes', 'Cardboard Reels', 'Boxes with special coatings', 'Boxes for batches', 'Clothing and textile boxes', 'Offset packaging', 'Ecommerce Boxes'];
    }
    if(selectedCategoria == 'Industrial Packaging') {
      listaDeSubcategorias = ['Large Industrial Packaging', 'Octabins for solids', 'Containers for liquids', 'Packaging kits'];
    }
    if(selectedCategoria == 'Plastic Packaging') {
      listaDeSubcategorias = ['Reusable boxes', 'Reusable containers', 'Packaging conditioners'];
    }
    if(selectedCategoria == 'Isothermal Packaging') {
      listaDeSubcategorias = ['Isothermal Boxes', 'ECO+', 'Isothermal Containers', 'EAcumulators'];
    }
    if(selectedCategoria == 'Protective Packaging') {
      listaDeSubcategorias = ['Anticorrosion Material', 'Filling and Protection', 'Molded Cellulose', 'Corner pieces and tubes', 'Paper adhesive tapes'];
    }
    if(selectedCategoria == 'Cardboard Pallets') {
      listaDeSubcategorias = ['CLICKpal', 'CLICKbox', 'CLICKair'];
    }
    if(selectedCategoria == 'Corrugated Cardboard') {
      listaDeSubcategorias = [];
    }
    if(selectedCategoria == '') {
      listaDeSubcategorias = todasLasSubcategorias_EN;
    }

    subcategoriaSelect.empty();
    subcategoriaSelect.append($("<option></option>").attr("value", "").text("All subcategories")); 
    
  }
  
  if(lang == "fr") { 
    if(selectedCategoria == 'Emballage pour les consommateurs') {
      listaDeSubcategorias = ['Boîtes en carton', 'Casiers à bouteilles', 'Boîtes pour marchandises dangereuses', 'Enrouleurs en carton', 'Boîtes avec des revêtements spéciaux', 'Boîtes pour les lots', 'Boîtes à vêtements et à textiles', 'Emballage offset', 'Boîtes de commerce électronique'];
    }
    if(selectedCategoria == 'Emballage industriel') {
      listaDeSubcategorias = ['Grands emballages industriels', 'Octabins pour solides', 'Récipients pour liquides', 'Kits d\'emballage'];
    }
    if(selectedCategoria == 'Emballage en plastique') {
      listaDeSubcategorias = ['Boîtes réutilisables', 'Conteneurs réutilisables', 'Conditionneurs d\'emballage'];
    }
    if(selectedCategoria == 'Emballage isotherme') {
      listaDeSubcategorias = ['Boîtes isothermes', 'ECO+', 'Conteneurs isothermes', 'Accumulateurs'];
    }
    if(selectedCategoria == 'Emballage de protection') {
      listaDeSubcategorias = ['Matériau anticorrosion', 'Remplissage et protection', 'Fibre moulée', 'Protections d\'angle et protections d\'angle', 'Rubans adhésifs en papier'];
    }
    if(selectedCategoria == 'Palettes en carton') {
      listaDeSubcategorias = ['CLICKpal', 'CLICKbox', 'CLICKair'];
    }
    if(selectedCategoria == 'Carton ondulé') {
      listaDeSubcategorias = [];
    }
    if(selectedCategoria == '') {
      listaDeSubcategorias = todasLasSubcategorias_FR;
    }

    subcategoriaSelect.empty();
    subcategoriaSelect.append($("<option></option>").attr("value", "").text("Toutes les sous-catégories")); 
    
  }
  
  if(lang == "ca") { 
    if(selectedCategoria == 'Embalatge de Consum') {
      listaDeSubcategorias = ['Caixes de cartró', 'Caixes per ampolles', 'Caixes per a Mercaderies Perilloses', 'Carrets de cartró', 'Caixes cpn recobriments especials', 'Caixes per a Lots', 'Caixes per a roba i tèxtil', 'Packaging offset', 'Caixes per a ecommerce'];
    }
    if(selectedCategoria == 'Embalatge Industrial') {
      listaDeSubcategorias = ['Gran embalatge industrial', 'Octabins per a sòlids', 'Contenidors per a líquids', 'Kits d\'embalatge'];
    }
    if(selectedCategoria == 'Embalatge de Plàstic') {
      listaDeSubcategorias = ['Caixes reutilitzables', 'Contenidors reutilitzables', 'Condicionadors d\'embalatge'];
    }
    if(selectedCategoria == 'Embalatge Isotèrmic') {
      listaDeSubcategorias = ['Caixes Isotèrmiques', 'ECO+', 'Contenidors Isotèrmics', 'Acumuladors'];
    }
    if(selectedCategoria == 'Embalatge de protecció') {
      listaDeSubcategorias = ['Material Anticorrosió', 'Farciment i protecció', 'Cel·lulosa Emmotllada', 'Cantoneres i tubs', 'Cintas adhesivas de papel'];
    }
    if(selectedCategoria == 'Palets de Cartró') {
      listaDeSubcategorias = ['CLICKpal', 'CLICKbox', 'CLICKair'];
    }
    if(selectedCategoria == 'Cartró Ondulat') {
      listaDeSubcategorias = [];
    }
    if(selectedCategoria == '') {
      listaDeSubcategorias = todasLasSubcategorias_CA;
    }

    subcategoriaSelect.empty();
    subcategoriaSelect.append($("<option></option>").attr("value", "").text("Totes les subcategories")); 
    
  }
  
  
  $.each(listaDeSubcategorias, function(key, value) {
    subcategoriaSelect.append($("<option></option>").attr("value", value).text(value)); 
  });
  
});

// Filter
$("select").change(function(){
  
  var query = buildQuery();
  
  getProductos(query);
  
});

function unique(list) {
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
}