const axios = require("axios");

exports.main = async (context, sendResponse) => {
  let contactId = context.contact.vid;

  let config = {
    method: "get",
    url: `https://api.hubapi.com/crm/v3/objects/contacts/${contactId}?associations=companies&archived=false`,
    headers: {
      authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
    },
  };

  try {
    let response = await axios(config);
    let companyIds = response.data.associations.companies.results.map(
      (result) => result.id
    );
    let uniqueCompanyIds = [...new Set(companyIds)];
    let allData = [];

    for (let companyId of uniqueCompanyIds) {
      let config_associations = {
        method: "get",
        url: `https://api.hubapi.com/crm/v4/objects/companies/${companyId}/associations/presupestos`,
        headers: {
          authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
        },
      };
      response = await axios(config_associations);
      console.log(response.data.paging, "gettingId");
      let customObjectIds = response.data.results.map(
        (result) => result.toObjectId
      );
      let uniqueCustomObjectIds = [...new Set(customObjectIds)];
      // console.log(uniqueCompanyIds,'companies ID');
      let inputs = uniqueCustomObjectIds.map((id) => ({ id }));
      let data = JSON.stringify({
        inputs,
        properties: ["id_presupuesto", "titulo", "fecha", "empresa","documento","id_cliente_font"],
      });
      let config = {
        method: "post",
        url:
          "https://api.hubapi.com/crm/v3/objects/presupestos/batch/read?archived=false",
        headers: {
          authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
          "content-type": "application/json",
        },
        data: data,
      };

      axios
        .request(config)
        .then((response) => {
          // console.log(response.data,'testing')
          allData.push(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    }

    sendResponse({ body: {allData:allData,companyIds: uniqueCompanyIds}, statusCode: 200 });
  } catch (error) {
    console.error("Error:", error);
    sendResponse({
      body: {
        message: "An error occurred during the API request.",
        errorDetails: error,
      },
      statusCode: 500,
    });
  }
};
