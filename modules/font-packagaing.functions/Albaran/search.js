const axios = require("axios");
exports.main = async (context, sendResponse) => {
  let allData = [];
  //  console.log(context?.body)
  let data = JSON.stringify({
    // after: "1",
    filterGroups: [
      {
        filters: [
          {
            propertyName: "associations.1-329",
            operator: "IN",
            values: context?.body?.companyID,
          },
          {
            propertyName: context?.body?.propertyName,
            operator: "CONTAINS_TOKEN",
            value: `*${context?.body?.propertyValue}*`,
          },
        ],
      },
    ],
  });

  let config = {
    method: "post",
    url: "https://api.hubapi.com/crm/v3/objects/albaranes/search",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
    },
    data: data,
  };

  // sendResponse({body: {input:context?.body ,data:data, config:config}})
  axios
    .request(config)
    .then((response) => {
      console.log(allData, "search!!!!");
      allData.push(response.data);
      sendResponse({ body: allData });
    })
    .catch((error) => {
      console.log(error);
    });
};
