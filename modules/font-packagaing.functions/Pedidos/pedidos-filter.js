const axios = require("axios");

exports.main = async (context, sendResponse) => {
  let data = JSON.stringify({
    // after: "1",
    filterGroups: [
      {
        filters: [
          {
            propertyName: "associations.1-325",
            operator: "IN",
            values: context?.body?.companyId,
          },
          {
            propertyName: context?.body?.propertyName,
            operator: "CONTAINS_TOKEN",
            value: `*${context?.body?.propertyValue}*`,
          },
        ],
      },
    ],
  });

  const apiUrl = "https://api.hubapi.com/crm/v3/objects/pedidos/search";

  const requestBody = data;

  try {
    const response = await axios.post(apiUrl, requestBody, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6`,
      },
    });

    const data = response.data;
    sendResponse({ body: JSON.stringify(data), statusCode: 200 });
  } catch (error) {
    console.error("Error:", error);

    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal Server Error" }),
    };
  }
};
