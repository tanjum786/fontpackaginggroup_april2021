const axios = require("axios");

exports.main = async (context, sendResponse) => {
  // Check if the email exists in the input
  const userEmail = context.body.gestor_email;
  if (!userEmail) {
    console.error('No email provided');
    return sendResponse(JSON.stringify({ error: "No email provided" }), { statusCode: 400 });
  }
  
  try {
    // Attempt to retrieve HubSpot user ID from email
    const userId = await fetchUserId(userEmail);
    if (!userId) {
      console.error('No HubSpot user found for the provided email');
      return sendResponse(JSON.stringify({ error: "No HubSpot user found for the provided email" }), { statusCode: 404 });
    }
    
    // Create the ticket with the user association
    const ticketResponse = await createTicket(context.body, userId);
    sendResponse(JSON.stringify(ticketResponse.data));
  } catch (error) {
    console.error('Error:', error);
    sendResponse(JSON.stringify({ error: error.message }), { statusCode: 500 });
  }
};

async function fetchUserId(email) {
  const config = {
    method: "get",
    url: `https://api.hubapi.com/crm/v3/objects/contacts/search`,
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6", // Manage the token securely
      Accept: "application/json",
    },
    data: {
      filterGroups: [{
        filters: [{
          propertyName: "email",
          operator: "EQ",
          value: email
        }]
      }]
    }
  };

  const response = await axios.request(config);
  if (response.data.results.length > 0) {
    return response.data.results[0].id; // Assuming first match is the correct user
  } else {
    return null; // Return null if no user is found
  }
}

async function createTicket(body, userId) {
  const data = JSON.stringify({
    associations: [
      {
        to: {
          id: userId,
        },
        types: [
          {
            associationCategory: "HUBSPOT_DEFINED",
            associationTypeId: 337, // Confirm this type ID
          },
        ],
      },
    ],
    properties: {
      subject: body.subject,
      hs_pipeline_stage: 1,
      content: body.des,
    },
  });

  const config = {
    method: "post",
    url: "https://api.hubapi.com/crm/v3/objects/tickets",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
    },
    data: data,
  };

  return axios.request(config);
}
