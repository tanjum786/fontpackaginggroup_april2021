const axios = require("axios");
exports.main = async (context, sendResponse) => {
  let id = context?.params?.id[0];
  console.log(id, "----");
  let config = {
    method: "get",
    maxBodyLength: Infinity,
    url: `https://api.hubapi.com/crm/v3/objects/pedidos/${id}/associations/companies`,
    headers: {
      Authorization: "Bearer pat-na1-21d06844-5a02-493b-811f-921979ff8db6",
    },
  };

  axios
    .request(config)
    .then((response) => {
      let data = JSON.stringify(response.data);

      sendResponse({ body: data, statusCode: 200 });
    })
    .catch((error) => {
      console.log(error);
    });
};
