const axios = require("axios");

exports.main = async (context, sendResponse) => {
  let id = context?.params?.id[0];
  const axios = require("axios");

  let properties = [
    "empresa",
    "id_pedido_cliente",
    "referencia_del_cliente",
    "cantidad_pedido",
    "fecha_entrega",
    "estado",
    "id_pedido_font",
    "id_cliente_font",
    "linea_pedido_font",
    "cantidad_entregada",
    "id_articulo",
    "direccion_envio",
    "documento",


  ]; // Add your properties here

  let config = {
    method: "get",
    url: `https://api.hubapi.com/crm/v3/objects/pedidos/${id}?${properties
      .map((prop) => `properties=${prop}`)
      .join("&")}&archived=false`,
    headers: {
      "Content-Type": "application/json",
      authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
    },
  };

  axios
    .request(config)
    .then((response) => {
      const data = response.data;
      sendResponse({ body: JSON.stringify(data), statusCode: 200 });
    })
    .catch((error) => {
      console.log(error);
    });
};
