const axios = require("axios");
exports.main = async (context, sendResponse) => {
  let id = context.params.id[0];
  let data = JSON.stringify({
    properties: {
      fecha_entrega: context.body.propertyValue,
      // hubspot_owner_id: context.body.hubspot_owner_id,
    },
  });

  let config = {
    method: "patch",
    url: `https://api.hubapi.com/crm/v3/objects/p9149224_pedidos/${id}`,
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
    },
    data: data,
  };

  axios
    .request(config)
    .then((response) => {
      sendResponse(JSON.stringify(response.data));
    })
    .catch((error) => {
      console.log(error);
    });
};

