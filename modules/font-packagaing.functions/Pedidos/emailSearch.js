const axios = require('axios');
exports.main = async (context, sendResponse) => {
  console.log(context.body)
let data = JSON.stringify({
  "filterGroups": [
    {
      "filters": [
        {
          "propertyName": "email",
          "operator": "EQ",
          "value": context?.body?.gestorEmail
        }
      ]
    }
  ],
  "properties": [
    "email"
  ]
});

try {
  let config = {
    method: 'get',
    url: `https://api.hubapi.com/crm/v3/owners/?email=${context?.body?.gestorEmail}&limit=100&archived=false`,
    headers: { 
      'authorization': 'Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6'
    }
  };
  
  axios.request(config)
  .then((response) => {
    console.log(JSON.stringify(response.data));
    const id = response.data.results[0].id;
    console.log(id);
    sendResponse(JSON.stringify({id}));
  })
  .catch((error) => {
    console.log(error);
  });
} catch (error) {
  throw error;
}

};
