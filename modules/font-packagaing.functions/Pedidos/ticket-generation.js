const axios = require("axios");
exports.main = async (context, sendResponse) => {
  console.log(context.body.hubspot_owner_id)
  let data = JSON.stringify({
    associations: [
      {
        to: {
          id: context.body.id,
        },
        types: [
          {
            associationCategory: "USER_DEFINED",
            associationTypeId: 337,
          },
        ],
      },
      {
        to: {
          id: context.body.comId,
        },
        types: [
          {
            associationCategory: "HUBSPOT_DEFINED",
            associationTypeId: 26,
          },
        ],
      },
      {
        to: {
          id: context.body.contactId,
        },
        types: [
          {
            associationCategory: "HUBSPOT_DEFINED",
            associationTypeId: 16,
          },
        ],
      },
    ],
    properties: {
      subject: "Cambio Fecha de Entrega",
      hs_pipeline_stage: 1,
      content: context.body.des,
      hubspot_owner_id: context.body.hubspot_owner_id
    },
  });

  let config = {
    method: "post",
    url: "https://api.hubapi.com/crm/v3/objects/tickets",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
    },
    data: data,
  };

  axios
    .request(config)
    .then((response) => {
      sendResponse(JSON.stringify(response.data));
    })
    .catch((error) => {
      console.log(error);
    });
};
