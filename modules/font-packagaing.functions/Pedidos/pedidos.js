const axios = require("axios");

exports.main = async (context, sendResponse) => {
  let contactId = context.contact.vid;

  let config = {
    method: "get",
    url: `https://api.hubapi.com/crm/v3/objects/contacts/${contactId}?associations=companies&archived=false`,
    headers: {
      authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
    },
  };

  try {
    let response = await axios(config);
    let companyIds = response.data.associations.companies.results.map(
      (result) => result.id
    );
    let uniqueCompanyIds = [...new Set(companyIds)];
    let allData = [];
    for (let companyId of uniqueCompanyIds) {
      let config_associations = {
        method: "get",
        url: `https://api.hubapi.com/crm/v4/objects/companies/${companyId}/associations/pedidos`,
        headers: {
          authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
        },
      };
      response = await axios(config_associations);
      let customObjectIds = response.data.results.map(
        (result) => result.toObjectId
      );
      let uniqueCustomObjectIds = [...new Set(customObjectIds)];
      let inputs = uniqueCustomObjectIds.map((id) => ({ id }));
      let data = JSON.stringify({
        inputs,
        properties: [
          "empresa",
          "id_pedido_cliente",
          "referencia_del_cliente",
          "cantidad_pedido",
          "fecha_entrega",
          "estado",
          "id_cliente_font",
          "id_pedido_font",
          "linea_pedido_font",
          "id_articulo",
          "direccion_envio",
          "documento",
          "cantidad_entregada",
          "gestor_email"
        ],
      });

      let config = {
        method: "post",
        url:
          "https://api.hubapi.com/crm/v3/objects/pedidos/batch/read?archived=false",
        headers: {
          authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
          "content-type": "application/json",
        },
        data: data,
      };

      axios
        .request(config)
        .then((response) => {
          allData.push(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    sendResponse({
      body: { allData: allData, companyIds: uniqueCompanyIds,contactId:contactId },
      statusCode: 200,
    });
  } catch (error) {
    console.error("Error:", error);
    sendResponse({
      body: {
        message: "An error occurred during the API request.",
        errorDetails: error,
      },
      statusCode: 500,
    });
  }
};
