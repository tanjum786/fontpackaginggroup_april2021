const axios = require("axios");
exports.main = async (context, sendResponse) => {
 let id= context.body.id;
 let contactId = context.body.contactId;
 console.log(id,contactId,'showing something');
let data = JSON.stringify({
  "inputs": [
    {
      "from": {
        "id": id,
      },
      "to": {
        "id": contactId,
      }
    }
  ]
});

let config = {
  method: 'post',
  url: `https://api.hubapi.com/crm/v4/associations/pedidos/contact/batch/associate/default`,
  headers: { 
    'authorization': 'Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6', 
    'content-type': 'application/json'
  },
  data : data
};
axios.request(config)
.then((response) => {
    if(response.status === 204 || response.data === "") { 
        sendResponse({ status: 'success', message: 'Association created successfully', data: null });
    } else {
        sendResponse({ status: 'success', data: response.data });
    }
})
.catch((error) => {
    console.error(error);
    sendResponse({ status: 'error', message: error.message });
});
}
