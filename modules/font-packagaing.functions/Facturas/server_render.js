const axios = require("axios");

exports.main = async (context, sendResponse) => {
  let id = context?.params?.id[0];
  const axios = require("axios");

  let properties = [
    "empresa",
    "id_factura",
    "id_cliente_font",
    "fecha",
    "importe",
  ]; // Add your properties here

  let config = {
    method: "get",
    url: `https://api.hubapi.com/crm/v3/objects/facturas/${id}?${properties
      .map((prop) => `properties=${prop}`)
      .join("&")}&archived=false`,
    headers: {
      "Content-Type": "application/json",
      authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
    },
  };

  axios
    .request(config)
    .then((response) => {
      const data = response.data;
      sendResponse({ body: JSON.stringify(data), statusCode: 200 });
    })
    .catch((error) => {
      console.log(error);
    });
};
