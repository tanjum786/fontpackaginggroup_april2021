const axios = require("axios");
exports.main = async (context, sendResponse) => {
  // console.log(context.params, "ids");
  let docId = context.params.id[0];
  console.log(docId, "testing");
  let config = {
    method: "get",
    url: `https://api.hubapi.com/files/v3/files/${docId}`,
    headers: {
      authorization: "Bearer pat-na1-ce487177-2f19-4e8d-8e04-bc5dd4a4d6f6",
    },
  };

  axios
    .request(config)
    .then((response) => {
      sendResponse(JSON.stringify(response.data));
    })
    .catch((error) => {
      console.log(error);
    });
};

//POst- body
//Get -param
