window.addEventListener("DOMContentLoaded", (event) => {
  var url = new URL(window.location.href);
  var params = new URLSearchParams(url.search);
  var searchParam = params.get("search");
  if (searchParam) {
    document.getElementById("searchBar").value = searchParam;
  }
});

//search
document.getElementById("searchBar").addEventListener("keydown", function(e) {
  if (e.key === "Enter") {
    e.preventDefault();
    var url = new URL(window.location.href);
    var params = new URLSearchParams(url.search);
    params.set("search", this.value);
    params.delete("offset"); // remove the offset parameter
    url.search = params.toString();
    window.location.href = url.toString();
  }
});

//download single pdf
document.body.addEventListener("click", function(event) {
  if (event.target.classList.contains("download_pdf")) {
    event.preventDefault();
    var pdfUrl = event.target.getAttribute("data-url");
    var pdfName = event.target.getAttribute("data-name");
    fetch(pdfUrl)
      .then((resp) => resp.blob())
      .then((blob) => {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement("a");
        a.style.display = "none";
        a.href = url;
        // the filename you want
        a.download = pdfName + ".pdf";
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(() => alert("Something went wrong!"));
  }
});

var counterElement = document.getElementById("counter");
var checkboxes = document.getElementsByClassName("selectRow");

//counter code
function updateCounter() {
  var count = 0;
  for (var i = 0; i < checkboxes.length; i++) {
    if (checkboxes[i].checked) {
      count++;
    }
  }
  counterElement.textContent = count;
}

for (var i = 0; i < checkboxes.length; i++) {
  checkboxes[i].addEventListener("change", updateCounter);
}

//select all checkbox
document.getElementById("selectAll").addEventListener("change", function() {
  for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].checked = this.checked;
  }
  updateCounter();
});

//download all selected pdfs
// document
//   .getElementById("downloadAll")
//   .addEventListener("click", function(event) {
//     event.preventDefault();
//     var checkboxes = document.getElementsByClassName("selectRow");
//     var zip = new JSZip();
//     var zipFilename = "pdfs.zip";
//     var fetchPromises = [];

//     for (var i = 0; i < checkboxes.length; i++) {
//       if (checkboxes[i].checked) {
//         (function(i) {
//           var row = checkboxes[i].closest(".table-wrapper-document");
//           var downloadLink = row.querySelector(".download_pdf");
//           var pdfUrl = downloadLink.getAttribute("data-url");
//           var row_name = row.querySelector(".row_name");
//           var file_name = row_name.getAttribute("data-name");
//           var timestamp = new Date().getTime();
//           var pdfName = "file_" + file_name + timestamp + "_" + i;

//           // Push each fetch promise into an array
//           fetchPromises.push(
//             fetch(pdfUrl)
//               .then((resp) => resp.blob())
//               .then((blob) => {
//                 zip.file(pdfName + ".pdf", blob, { binary: true });
//               })
//           );
//         })(i);
//       }
//     }

//     Promise.all(fetchPromises)
//       .then(() => {
//         zip.generateAsync({ type: "blob" }).then(function(content) {
//           setTimeout(function() {
//             saveAs(content, zipFilename);
//           }, 2000);
//         });
//       })
//       .catch(() => alert("Something went wrong!"));
//   });
document
  .getElementById("downloadAll")
  .addEventListener("click", function(event) {
    event.preventDefault();
    var checkboxes = document.getElementsByClassName("selectRow");
    var zip = new JSZip();
    var zipFilename = "pdfs.zip";
    var fetchPromises = [];

    for (var i = 0; i < checkboxes.length; i++) {
      if (checkboxes[i].checked) {
        (function(i) {
          var row = checkboxes[i].closest(".table-wrapper-document");
          var downloadLink = row.querySelector(".download_pdf");
          var row_name = row.querySelector(".row_name");

          // Check if downloadLink and row_name are not null
          if (downloadLink && row_name) {
            var pdfUrl = downloadLink.getAttribute("data-url");
            var file_name = row_name.getAttribute("data-name");
            var timestamp = new Date().getTime();
            var pdfName = "file_" + file_name + timestamp + "_" + i;

            // Check if the data-url attribute is not empty
            if (pdfUrl) {
              // Push each fetch promise into an array
              fetchPromises.push(
                fetch(pdfUrl)
                  .then((resp) => resp.blob())
                  .then((blob) => {
                    zip.file(pdfName + ".pdf", blob, { binary: true });
                  })
              );
            }
          }
        })(i);
      }
    }

    Promise.all(fetchPromises)
      .then(() => {
        zip.generateAsync({ type: "blob" }).then(function(content) {
          setTimeout(function() {
            saveAs(content, zipFilename);
          }, 2000);
        });
      })
      .catch(() => alert("Something went wrong!"));
  });


// Filter drop
 document.addEventListener('DOMContentLoaded', function() {
  // Set the dropdown value based on the URL
  var urlParams = new URLSearchParams(window.location.search);
  
  var docType = urlParams.get('docType');
  if (docType) {
    document.getElementById('dropdown').value = docType;
  } else {
    document.getElementById('dropdown').value = 'empresa';
  }

  var sortOrder = urlParams.get('orderBy');
  if (sortOrder === 'fecha') {
    document.getElementById('dropdown_1').value = 'ascending';
  } else if (sortOrder === 'fecha__desc') {
    document.getElementById('dropdown_1').value = 'descending';
  } else {
    document.getElementById('dropdown_1').value = 'empresa'; 
  }
});

// Filter for Tipo de documento

document.getElementById('dropdown').addEventListener('change', function() {
  var docType = this.value;
  console.log("Document type: " + docType);
  var limit = 5; 
  var offset = 0; 

  var query = "limit=" + limit + "&offset=" + offset;

  if (docType !== 'empresa') { 
      query += "&docType=" + docType;
  }

  console.log("New query: " + query); 
  window.location.search = query;
});

 //filter-fecha

document.getElementById('dropdown_1').addEventListener('change', function() {
  var sortOrder = this.value;
  console.log("Sort order: " + sortOrder);
  var limit = 5; 
  var offset = 0; 

  var query = "limit=" + limit + "&offset=" + offset;

  if (sortOrder === 'ascending') { 
    query += "&orderBy=fecha";
  } else if (sortOrder === 'descending') { 
    query += "&orderBy=fecha__desc";
  }

  console.log("New query: " + query); 
  window.location.search = query;
});
