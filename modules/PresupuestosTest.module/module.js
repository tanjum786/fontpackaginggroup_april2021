// Function to update the table
function updateTable(data) {
  try {
    const table = document.getElementById("data-table");
    //   console.log("Updating table with data:", data);
    const tbody = table.getElementsByTagName("tbody")[0];
    tbody.innerHTML = ""; // Clear existing rows

    data.forEach((item) => {
      const row = tbody.insertRow();
      row.insertCell(0).innerHTML = item?.properties?.empresa || "-";
      row.insertCell(1).innerHTML = item?.properties?.id_presupuesto || "-";
      row.insertCell(2).innerHTML = item?.properties?.titulo || "-";
      row.insertCell(3).innerHTML = item?.properties?.fecha || "-";
      row.insertCell(4).innerHTML = item?.id || "-";
      // Add the documento link if available
      const documentoCell = row.insertCell(5);
      if (item?.documento) {
        documentoCell.innerHTML = `<a href="${item.documento}" download>Download</a>`;
      } else {
        documentoCell.innerHTML = "-";
      }
    });
  } catch (error) {
    console.error("Error updating table:", error);
  }
}

// const updatedObject = (id) => {
//   fetch(
//     `https://fontpackaging.com/_hcms/api/server_render?portalid=9149224&id=${id}`
//   )
//     .then((response) => response.json())
//     .then((data) => {
//       console.log(data, "checking");
//       let dataArray = [data];
//       updateTable(dataArray);
//     })
//     .catch((err) => {
//       console.log(err);
//     });
// };

// Fetch initial data

const updatedObject = (id) => {
  return fetch(
    `https://fontpackaging.com/_hcms/api/server_render?portalid=9149224&id=${id}`
  )
    .then((response) => response.json())
    .catch((err) => {
      console.error(err);
      return null;
    });
};

let companyId = [];

const customObject = () => {
  fetch("https://fontpackaging.com/_hcms/api/presupuestos?portalid=9149224")
    .then((response) => response.json())
    .then((data) => {
      // Flatten data if nested
      if (Array.isArray(data) && Array.isArray(data[0])) {
        data = data.flat();
      }

      companyId = data.companyIds;
      const fetchDataForDocuments = data.allData.flatMap((item) =>
        item.results.map((result) => {
          // If the result has a 'properties.documento', fetch its details
          if (result.properties && result.properties.documento) {
            const documentoId = result.properties.documento;
            return fetch(
              `https://fontpackaging.com/_hcms/api/download?portalid=9149224&id=${documentoId}`,
              {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
              }
            )
              .then((response) => response.json())
              .then((data) => {
                const parsedData = JSON.parse(data);
                return { ...result, documento: parsedData.url }; // Merge the document URL into the result
              })
              .catch((error) =>
                console.error("Error fetching document details:", error)
              );
          } else {
            return Promise.resolve(result); // Return the result as is if no documento
          }
        })
      );

      // Wait for all document fetches to complete
      Promise.all(fetchDataForDocuments).then((allResults) => {
        updateTable(allResults);
      });
    })
    .catch((error) => console.error("Error:", error));
};

customObject();

// Search
document.getElementById("searchBar").addEventListener("input", function () {
  let searchValue = this.value; // Get the search value
  if (searchValue.length === 0) {
    customObject();
  } else {
    // List of property names to search
    let propertyNames = ["empresa", "id_presupuesto", "titulo", "fecha"];

    //    console.log(companyId);
    // Fetch data for each property name
    propertyNames.forEach((propertyName) => {
      fetch("https://fontpackaging.com/_hcms/api/search?portalid=9149224", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          propertyName: propertyName,
          propertyValue: searchValue,
          companyId,
        }),
      })
        .then((response) => response.json())
        // console.log(response)
        // .then((data) => {
        //   if (data[0]?.results?.length === 0) {
        //     updateTable((data = []));
        //   } else {
        //     if (data[0]?.results?.length > 0) {
        //       data[0]?.results?.forEach((item) => {
        //         console.log(item.id);
        //         updatedObject(item.id);
        //       });
        //     }
        //   }
        // })
        // Inside the search event listener, after fetching search results
        .then((data) => {
          // Assuming data[0] exists and has a results array
          const idsToFetch = data[0]?.results.map((item) => item.id) || [];

          let fetchPromises = idsToFetch.map((id) => updatedObject(id));

          Promise.all(fetchPromises)
            .then((allData) => {
              // console.log(allData); 
              updateTable(allData.filter((item) => item)); // Update the table with the fetched data, filtering out nulls if any fetch failed
            })
            .catch((error) => {
              console.error(error);
            });
        })

        .catch((error) => {
          console.log(error);
        });
    });
  }
});

//Filter
// Update the dropdown change event listener
document.getElementById("dropdown").addEventListener("change", function () {
  let selectedValue = this.value; // Get the selected value
  let filterValue = "";
  console.log(selectedValue, "selcetdValue");
  if (selectedValue === "font") {
    filterValue = "CF";
  } else if (selectedValue === "troquel") {
    filterValue = "TS";
  }

  if (filterValue) {
    // If "font" or "troquel" is selected, filter the table to show only rows where empresa is "CF" or "TS"
    // fetch("https://fontpackaging.com/_hcms/api/filter?portalid=9149224", {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/json",
    //   },
    //   body: JSON.stringify({
    //     propertyName: "empresa",
    //     propertyValue: filterValue,
    //     companyId,
    //   }),
    // })
    //   .then((response) => response)
    //   .then((data) => {
    //     console.log(data)
    //     if (data[0]?.results.length === 0) {
    //       updateTable((data = []));
    //     } else {
    //       if (data[0]?.results?.length > 0) {

    //         data[0]?.results?.forEach((item) => {
    //           console.log(item.id);
    //           updatedObject(item.id);
    //         });
    //       }
    //     }
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
    fetch("https://fontpackaging.com/_hcms/api/filter?portalid=9149224", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        propertyName: "empresa",
        propertyValue: filterValue,
        companyId,
      }),
    })
      .then((response) => response.json()) // Convert the response body to JSON
      // .then((data) => {
      //   console.log(data); // Now 'data' is the parsed JSON object
      //   if (data.results.length === 0) {
      //     updateTable([]); // Update the table with an empty array if no results
      //   } else {
      //     data.results.forEach((item) => {
      //       console.log(item.id);
      //       updatedObject(item.id); // Assuming 'updatedObject' properly handles each item
      //     });
      //   }
      // })

      .then((data) => {
        console.log(data);
        if (data.results.length === 0) {
          updateTable([]); // Update the table with an empty array if no results
        } else {
          const idsToFetch = data.results.map((item) => item.id);

          let fetchPromises = idsToFetch.map((id) => updatedObject(id));

          Promise.all(fetchPromises)
            .then((allData) => {
              // console.log(allData); 
              updateTable(allData.filter((item) => item)); // Update the table with the fetched data, filtering out nulls if any fetch failed
            })
            .catch((error) => {
              console.error(error); 
            });
        }
      })

      .catch((error) => {
        console.error(error);
      });
  } else {
    // If other option is selected, revert to the original table data
    customObject();
  }
});
